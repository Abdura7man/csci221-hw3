#include <iostream>
using namespace std;

struct node{
	int data;
	struct node* next;
};
void RemoveNode(struct node** headRef, int dataValue);
// Method 1
void sortedDuplicate(struct node* head) {
	if(head == NULL) return;							

	struct node* current = head;					

	while(current->next != NULL) {						
		
		if(current->data == current->next->data) {		
			struct node* temp = current->next->next;	
			delete current->next;						
			current->next = temp;						

		 
		} else {
			current = current->next;					
		}
	}
} 
// Method 2
void unsortedDuplicateV1(struct node* head) {
	if(head == NULL) return;							

	struct node* current = head;	
    struct node* runner = NULL;

    while(current->next != NULL) {
		runner = current->next;							
		
		while(runner != NULL) {							
			if(runner->data == current->data) {			
				RemoveNode(&(current->next), current->data);		
			}
			runner = runner->next;						
		}

		current = current->next;						
	}
}
	
// method 3
void unsortedDuplicateV2(struct node* head) {
	if(head == NULL) return;							

	struct node* current = head;						
	struct node* runner = NULL;							
	struct node* duplicate = NULL;						

	while(current->next != NULL) {
		runner = current;								

		while (runner->next != NULL) {					
			if(current->data == runner->next->data) {	
				duplicate = runner->next;				
				runner->next = runner->next->next;		
				delete duplicate;						

			
			} else {
				runner = runner->next;					
			}
		}

		current = current->next;						
	}
}

void Push(struct node** headRef, int dataValue){
	struct node* newNode = new struct node;
	newNode->data = dataValue;
	newNode->next = *headRef;
	*headRef = newNode;
}

void Print(struct node* head){
	struct node* current = head;

	while(current != NULL){
		cout << current->data << endl;
		current = current->next;
	}
}

int Length(struct node* head){
	int count = 0;
	struct node* current = head;

	while(current != NULL){
		//cout << current->data << endl;
		count++;
		current = current->next;
	}
	return count;
}

void AddToEnd1(struct node** headRef, int dataValue){
	struct node* newNode = new struct node;
	newNode->data = dataValue;
	if( (*headRef) != NULL){
		struct node* current = *headRef;
		while(current->next != NULL){
			current = current->next;
		}
		newNode->next = current->next;
		current->next = newNode;
	}else{ // The list is empty!
		newNode->next = NULL;
		*headRef = newNode;
	}

}

void AddToEnd2(struct node** headRef, int dataValue){
	struct node* current = *headRef;
	if(current == NULL){
		Push(headRef, dataValue);
	}else{
		while(current->next != NULL){
			current = current->next;
		}
		Push(&(current->next),dataValue);
	}

}

void RemoveNode(struct node** headRef, int dataValue){
	struct node* current = *headRef;
	struct node* prev = NULL;

	while( current != NULL ){

		if(current->data == dataValue){ //check if the node is found
			if(current == *headRef){
				*headRef = current->next;
			}else{
				prev->next = current->next;
				delete current;
				return;
			}
		}
		prev = current;
		current = current->next;
	}
}

struct node* FindKthLast(struct node* head, int k){
	int count = 0;
	int iterate = 0;
	struct node* current = head;

	while(current != NULL){
		count ++;
		current = current->next;
	}
	iterate = count - k;
	if(iterate < 0) {
		cout << "k is bigger than length of the list\n";
		return NULL;
	}

	current = head;
	while(iterate > 0){

		iterate --;
		current = current->next;
	}
	return current;

}


struct node* FindKthLastPointers(struct node* head, int k){
	struct node* current = head;
	struct node* runner = head;

	while( (runner != NULL) && (k>0) ){
		runner = runner->next;
		k--;
		if ((runner == NULL) && (k>0) ){
			cout << "k is bigger than length of the list\n";
			return NULL;
		}
		runner = runner->next;
		current = current->next;
	}

	while(runner != NULL){
		runner = runner->next;
		current = current->next;
	}

	return current;
}

void Reverse(struct node** headRef){
	struct node* newlist = NULL;
	struct node* current = *headRef;
	struct node* next;

	while(current != NULL){
		next = current->next;
		current->next = newlist;
		newlist = current;
		current = next;

	}
	*headRef = newlist;
}


void InsertSorted(struct node** headRef, int dataValue){
	struct node* newNode = new struct node;
	newNode->data = dataValue;

	struct node* current = *headRef;

	if( ((*headRef) == NULL) || newNode->data <= (*headRef)->data  ){  //for the first and second (if it is smaller than the first one)
		newNode->next = current;
		*headRef = newNode;

	}else{

		while( (current->next != NULL ) && ((current->next)->data <= newNode->data)){
			current = current->next;
		}
		newNode->next = current->next;
		current->next = newNode;
	}
}


int main(){
	struct node* head = NULL;
	//InsertSorted(&head, 90);
	//InsertSorted(&head, 80);
	//InsertSorted(&head, 70);

    Push(&head, 60);
	Push(&head, 60);
	Push(&head, 50);
	Push(&head, 50);
	Push(&head, 50);
	Push(&head, 42);

    cout<<"Before calling Method-1 : \n";
	Print(head);
	cout<<endl;

	sortedDuplicate(head);

	cout<<"After calling Method-1 : \n";
	Print(head);
	cout<<endl;
    struct node* head1 = NULL;

    Push(&head1, 9);
	Push(&head1, 50);
	Push(&head1, 42);
	Push(&head1, 19);
	Push(&head1, 50);
	Push(&head1, 42);

	
    cout<<"Before calling Method-2 : \n";
	Print(head1);
	cout<<endl;

	unsortedDuplicateV1(head1);

	cout<<"After calling Method-2 : \n";
	Print(head1);
	cout<<endl;
    
    struct node* head2 = NULL;

	Push(&head2, 9);
	Push(&head2, 50);
	Push(&head2, 42);
	Push(&head2, 19);
	Push(&head2, 50);
	Push(&head2, 42);

	cout<<"Before calling Method-3 : \n";
	Print(head2);
	cout<<endl;

	unsortedDuplicateV2(head2);

	cout<<"After calling Method-3 : \n";
	Print(head2);
	cout<<endl;

    // Print(head);
	// cout << endl;

	//RemoveNode(&head, 30);
	//Print(head);
	//cout << endl;

	//struct node* temp = FindKthLastPointers(head, 2);
	//if(temp != NULL)
	//	cout << "2nd from last: " <<
	//		temp->data <<endl;

	/*
	cout << "Length is : " << Length(head);
	cout << endl;
	//AddToEnd1(&head,50);
	Print(head);
	cout << endl;
	cout << "Length is : " << Length(head);
	cout << endl;
	AddToEnd2(&head,90);
	Print(head);
	cout << endl;
	cout << "Length is : " << Length(head);
	cout << endl;
	 */
	return 0;
}